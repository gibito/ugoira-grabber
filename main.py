import os
import re
import requests
import json
import zipfile
import threading
from math import ceil
from time import sleep
from wand_edited.image import Image
from uuid import uuid4
from shutil import copy, rmtree
from os.path import isdir
from sys import argv
from tempfile import gettempdir, mkdtemp
import userdata
from PyQt5 import QtWidgets, QtCore
import mainwindow

name = "ugoira-grabber"
cfg_filename = name + '.cfg'
cfg_path = os.path.join(userdata.getUserData(), cfg_filename)


class App(QtWidgets.QMainWindow, mainwindow.Ui_MainWindow):
    step_signal = QtCore.pyqtSignal()
    done_signal = QtCore.pyqtSignal(bool)
    err_signal = QtCore.pyqtSignal(str)

    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.toolButton.clicked.connect(self.select_folder)
        self.pushButton.clicked.connect(self.download)
        try:
            os.path_ = open(cfg_path).read()
        except:
            os.path_ = os.path.join(userdata.getHome(), "Downloads")
            open(cfg_path, 'w').write(os.path_)

        self.downloadsText.setText(os.path_)
        self.step_signal.connect(self.step)
        self.done_signal.connect(self.work_done)
        self.err_signal.connect(self.err)

    def select_folder(self):
        self.downloadsText.setText(
            QtWidgets.QFileDialog.getExistingDirectory(self, "Select downloads folder"))
        open(cfg_path, 'w').write(self.downloadsText.text())

    def download(self):
        self.pushButton.setEnabled(False)
        self.working_thread = threading.Thread(
            target=lambda: (self.done_signal.emit(self.work())))
        self.working_thread.start()

    @QtCore.pyqtSlot(bool)
    def work_done(self, success):
        self.pushButton.setEnabled(True)
        if success:
            QtWidgets.QMessageBox.about(
                self, "Done!", "All of your aniamtions have been downloaded\nEnjoy!")

    def work(self):
        self.progressBar.setValue(0)
        animations = self.animationsList.toPlainText()
        dir = self.downloadsText.text()
        if animations and dir:
            self.links = re.split('\n', animations)
            self.links = delete_empty(self.links)
            if len(self.links) < 1:
                self.err_signal.emit('No links in there')
                return False
            if len(self.links) == 1 and not self.links[0]:
                self.err_signal.emit('No links in there')
                return False
            self.done = 0
            for link in self.links:
                try:
                    page = requests.get(link).content.decode()
                except:
                    self.err_signal.emit("Could not load the page")
                    return False
                try:
                    json_ = json.loads(re.search('pixiv\.context\.ugokuIllustData.*?\=.*{.*?}]}', page)
                                       .group().replace('pixiv.context.ugokuIllustData  =', ''))
                except:
                    self.err_signal.emit(
                        "Could not find the animation on the given page")
                    return False

                archive_path = gettempdir() + '/' + name + '/' + str(self.done) + ".zip"
                try:
                    with open(archive_path, 'wb') as f:
                        f.write(requests.get(json_['src'], headers={
                                'Referer': link}).content)
                except:
                    self.err_signal.emit("Couldn't download the animation")
                    return False

                try:
                    self.convert(json_, archive_path,
                                 self.downloadsText.text())
                except:
                    self.err_signal.emit("Couldn't convert to gif")
                    return False

                os.remove(archive_path)
                self.step_signal.emit()
        elif not animations:
            self.err_signal.emit(
                "You should enter at least one link to download")
            return False
        else:
            self.err_signal.emit(
                "You should enter where do you want your animations to be downloaded to")
            return False
        return True

    @QtCore.pyqtSlot(str)
    def err(self, msg):
        QtWidgets.QMessageBox.about(self, "Error", msg)

    def convert(self, obj, src, dst):
        try:
            folder = mkdtemp()
            output = os.path.join(dst, str(uuid4()) + '.gif')
            zipfile.ZipFile(src, 'r').extractall(folder)

            self.step_signal.emit()

            wand = Image()
            for i in obj['frames']:
                frame = Image(filename=os.path.join(folder, i['file']))
                frame.delay = i['delay']
                wand.sequence.append(frame)

            wand.compression_quality = 100
            wand.save(filename=output)

            self.step_signal.emit()

            rmtree(folder)
        except Exception as e:
            print(e.args)

    @QtCore.pyqtSlot()
    def step(self):
        self.done += 1
        src = (self.done - 1) * 100 / (len(self.links) * 3)
        dst = self.done * 100 / (len(self.links) * 3)

        for i in range(ceil(src), ceil(dst)):
            self.progressBar.setValue(i)
            sleep(.01)

        self.progressBar.setValue(dst)
        

def delete_empty(arr):
    for element in arr:
        if element is '' or \
           element is '\n' or \
           element is '\r' or \
           element is ' ' or \
           element is '\r\n':
            arr.remove(element)
    return arr


def main():
    if not isdir(gettempdir() + '/' + name):
        os.makedirs(gettempdir() + '/' + name)

    app = QtWidgets.QApplication(argv)
    window = App()
    window.show()
    app.exec()


if __name__ == '__main__':
    main()
