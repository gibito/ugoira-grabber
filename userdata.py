import os

def getHome():
    if os.name == 'nt':
        return os.environ['USERPROFILE']
    elif os.name == 'posix':
        return os.environ['HOME']
    elif os.name == 'mac':
        return os.environ['HOME']

def getUserData():
    if os.name == 'nt':
        return os.environ['LOCALAPPDATA']
    elif os.name == 'posix':
        return os.environ['HOME'] + '/.local'
    elif os.name == 'mac':
        return os.environ['HOME'] + '/Library/Application Support'
